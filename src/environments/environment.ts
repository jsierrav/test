// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCh-8uxni7Eqbxz2AOrNBNnyMDHTl1TWqU",
    authDomain: "projectprueba-1470332633694.firebaseapp.com",
    databaseURL: "https://projectprueba-1470332633694.firebaseio.com",
    projectId: "projectprueba-1470332633694",
    storageBucket: "projectprueba-1470332633694.appspot.com",
    messagingSenderId: "49214723294"
  }
};
