import { Observable } from 'rxjs/Rx';
import { AngularFireList } from 'angularfire2/database/interfaces';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database-deprecated';

import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;
  users: IUser[];
  usersObservable: Observable<IUser[]>;

  user: IUser = {
    name: '',
    password: ''
  };

  @Output() action = new EventEmitter();

  constructor(private af: AngularFireDatabase) {
    this.itemsRef = af.list('users');
    // Use snapshotChanges().map() to store the key
    this.items = this.itemsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
  }

  ngOnInit() {
    this.usersObservable = this.af.list('users').valueChanges();
    this.usersObservable.subscribe(result => {
      this.users = result;
    })

  }

  login() {


    if (this.exist(this.user)) {

      this.action.emit({ user: this.user });


    } else {
      alert("No se encuentra el usuario")
    }
  }


  exist(user: IUser) {
    console.log(this.users, user);
    for (let i = 0; i < this.users.length; i++) {
      if (this.users[i].name == user.name && this.user.password == user.password) {
        return true;
      }
    }

    return false;
  }


  addUser() {

    this.itemsRef.push({ name: this.user.name, password: this.user.password });
    alert("Usuario registrado");
  }




}
export interface IUser {
  name?: string;
  password?: string;
}
