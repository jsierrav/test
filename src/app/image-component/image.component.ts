import { Component, OnInit } from '@angular/core';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database-deprecated';
import { AngularFireList, AngularFireObject } from 'angularfire2/database/interfaces';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Rx';


@Component({
    templateUrl: 'image.component.html',
    selector: 'app-image'
})

export class ImageComponent implements OnInit {

    public doughnutChartLabels: string[] = ['Download Sales', 'In-Store Sales'];
    public doughnutChartData: number[] = [0, 0];
    public doughnutChartType: string = 'doughnut';

    itemsRef: AngularFireList<any>;
    items: Observable<any[]>;
    imagesObservable: Observable<any[]>;
    images: any[];
    statsObservable: Observable<any[]>;
    stats: any[];
    show: boolean = false;

    constructor(private af: AngularFireDatabase) {


        this.imagesObservable = this.af.list('images').valueChanges();
        this.statsObservable = this.af.list('user-images').valueChanges();


        this.itemsRef = af.list('user-images');
        // Use snapshotChanges().map() to store the key
        this.items = this.itemsRef.snapshotChanges().map(changes => {
            return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
    }


    ngOnInit() {


        this.imagesObservable.subscribe(result => {
            this.images = result;


        })

        this.statsObservable.subscribe(result => {
            this.stats = result;
        })
    }

    like(image: any) {

        this.itemsRef.push({ idImage: image.id, userId: "jsierrav14@hotmail.com" });

        this.imagesObservable.subscribe(result => {
            this.images = result;


        })

        this.statsObservable.subscribe(result => {
            this.stats = result;
        })

    }


    showStats() {
        if (!this.show) {
            this.show = true;
        } else {
            this.show = false;

        }
        console.log(this.stats);
        for (let i = 0; i < this.images.length; i++) {
            this.doughnutChartData[i] = this.count(this.images[i].id);
            this.doughnutChartLabels[i] = this.images[i].name;
        }

        console.log(this.doughnutChartData)

    }

    count(id: number) {
        let cont = 0;

        for (let i = 0; i < this.stats.length; i++) {
            if (this.stats[i].idImage == id) {
                cont++;

            }
        }

        return cont;
    }

    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }
}