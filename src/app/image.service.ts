import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class ImageService {

  images:Observable<any[]>;

  constructor(private af: AngularFirestore) { }

  getImg() {
    
    this.images = this.af.collection('images').valueChanges();
    return this.images;

  }

}
