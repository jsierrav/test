import { Observable } from 'rxjs/Rx';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
@Component({
    templateUrl: 'statictics.component.html',
    selector: 'app-stats'
})

export class StaticticsComponent implements OnInit {

    public doughnutChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData:number[] = [350, 450, 100];
  public doughnutChartType:string = 'doughnut';

    imagesObservable: Observable<any[]>;
    images: any[];
    statsObservable: Observable<any[]>;
    stats: any[];

    constructor(private af: AngularFireDatabase) {
        this.imagesObservable = this.af.list('images').valueChanges();
        this.statsObservable = this.af.list('user-images').valueChanges();


    }

    ngOnInit() {

        this.imagesObservable.subscribe(result => {
            this.images = result;


        })

        this.statsObservable.subscribe(result => {
            this.stats = result;
        })

            console.log(this.images);

   

    }

    count(id: number) {
        let cont = 0;

        for (let i = 0; i < this.stats.length; i++) {
            if (this.stats[i].id == id) {
                cont++;
            }
        }

        return cont;
    }

    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }
}
